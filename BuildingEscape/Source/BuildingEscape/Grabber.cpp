// Copyright Aljaz Spindler

#include "Grabber.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}


// Called when the game starts
void UGrabber::BeginPlay()
{
	Super::BeginPlay();
	FindPhysicsHandleComponent();
	SetupInputComponent();
}

void UGrabber::FindPhysicsHandleComponent()
{
	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();
	FString ParentObject = GetOwner()->GetName();
	if (PhysicsHandle == nullptr)
	{
		UE_LOG(LogTemp, Error, TEXT("The Physics handle is missing at object %s"), *ParentObject);
	}
}

void UGrabber::SetupInputComponent()
{
	FString ParentObject = GetOwner()->GetName();
	InputHandle = GetOwner()->FindComponentByClass<UInputComponent>();
	if (InputHandle)
	{
		InputHandle->BindAction("Grab", EInputEvent::IE_Pressed, this, &UGrabber::Grab);
		InputHandle->BindAction("Grab", EInputEvent::IE_Released, this, &UGrabber::Release);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("The Input handle is missing at object %s"), *ParentObject);
	}
}

const FHitResult UGrabber::GetFirstBodyInReach()
{
	FVector Loc;
	FRotator Rot;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT Loc, OUT Rot);
	FVector LineTraceEnd = GetLineTraceEnd();

	// Setup query params.
	FCollisionQueryParams TraceParams(FName(TEXT("")), false, GetOwner());

	FHitResult Hit;
	GetWorld()->LineTraceSingleByObjectType(
		OUT Hit,
		Loc,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		TraceParams
	);

	return Hit;
}

const FVector UGrabber::GetLineTraceEnd()
{
	FVector Loc;
	FRotator Rot;

	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(OUT Loc, OUT Rot);

	FString ActorLocation = Loc.ToString();
	FString ActorRotation = Rot.ToString();
	FVector LineTraceDirection = Rot.Vector() * Reach;
	FVector LineTraceEnd = Loc + LineTraceDirection;

	return LineTraceEnd;
}

void UGrabber::Grab()
{
	// Line trace and find object if there.
	auto HitResult = GetFirstBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
	auto ActorHit = HitResult.GetActor();
	
	if (ActorHit)
	{
		if (!PhysicsHandle)
		{
			return;
		}
		PhysicsHandle->GrabComponentAtLocationWithRotation(ComponentToGrab, NAME_None, ComponentToGrab->GetOwner()->GetActorLocation(), ComponentToGrab->GetOwner()->GetActorRotation());
	}
}

void UGrabber::Release()
{
	// Release physics handle.
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (!PhysicsHandle)
	{
		return;
	}
	// If physics handle attached - move holding object.
	if (PhysicsHandle->GrabbedComponent)
	{
		PhysicsHandle->SetTargetLocation(GetLineTraceEnd());
	}
}

// Copyright Aljaz Spindler

#include "OpenDoor.h"
#include "GameFramework/Actor.h"
#include "Engine/World.h"
#include "Components/PrimitiveComponent.h"


// Sets default values for this component's properties
UOpenDoor::UOpenDoor()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOpenDoor::BeginPlay()
{
	Super::BeginPlay();
	Owner = GetOwner();
	if (!PressurePlate)
	{
		UE_LOG(LogTemp, Error, TEXT("The PressurePlate is missing!"));
	}
}


// Called every frame
void UOpenDoor::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (GetTotalMassOfActorsOnPlate() > TriggerMass)
	{
		OnOpen.Broadcast();
	}
	else
	{
		OnClose.Broadcast();
	}
}

float UOpenDoor::GetTotalMassOfActorsOnPlate()
{
	float Mass = 0.0f;

	TArray<AActor*> ActorsOnPressurePlate;
	if (!PressurePlate)
	{
		return 0.0f;
	}
	PressurePlate->GetOverlappingActors(OUT ActorsOnPressurePlate, nullptr);
	for (const auto& Actor : ActorsOnPressurePlate)
	{
		Mass += Actor->FindComponentByClass<UPrimitiveComponent>()->GetMass();
	}

	return Mass;
}

